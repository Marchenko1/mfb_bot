from django.contrib import admin
from .models import TgUser, CashFlow

admin.site.register(TgUser)
admin.site.register(CashFlow)

