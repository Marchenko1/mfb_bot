# Generated by Django 5.0.2 on 2024-02-10 18:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('Bot', '0006_alter_connectuser_connect_user'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ConnectUser',
        ),
    ]
