from django.db import models


class TgUser(models.Model):
    name = models.CharField(max_length=64, blank=True, null=True)
    user_name = models.CharField(max_length=64, blank=True, null=True)
    phone_number = models.CharField(max_length=64, blank=True, null=True)
    chat_id = models.IntegerField(blank=True, null=True)
    is_admin = models.BooleanField(default=False)

    def __str__(self):
        return self.user_name


class CashFlow(models.Model):
    quantity = models.IntegerField()
    m_from = models.ForeignKey(TgUser, on_delete=models.CASCADE, related_name='m_from')
    m_to = models.ForeignKey(TgUser, on_delete=models.CASCADE, related_name='m_to')
    description = models.CharField(max_length=1024, blank=True, null=True)
    date = models.DateTimeField()

    def __str__(self):
        return f'Передача от {self.m_from.name} к {self.m_to.name} денежных средств в размере {self.quantity}'
