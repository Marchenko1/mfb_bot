import asyncio

from django.core.management.base import BaseCommand
from django.conf import settings

import segno
import barcode

from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    MessageHandler,
    filters,
    ConversationHandler,
    CallbackQueryHandler,
)
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from barcode.writer import ImageWriter
from channels.db import database_sync_to_async
from asgiref.sync import sync_to_async
from Bot.models import TgUser, CashFlow

from django.core.exceptions import ObjectDoesNotExist

API_TOKEN = '6705183255:AAFUSPpYS0OBOGzLS4ngheWQ00nibk7lnUc'

CREATE_BARCOD, CREATE_QR, CREATE_EAN, MENU, MONEY = range(5)


def create_people(message):
    try:
        people = TgUser.objects.get(user_name=message.chat.username)
    except ObjectDoesNotExist:
        people = TgUser(
            name=message.chat.full_name,
            user_name=message.chat.username,
            chat_id=message.chat.id,
            is_admin=False
        )
        people.save()

    return people


def create_cashflow(message):
    list_message = message.text.split(maxsplit=2)
    pass
    cash = CashFlow(

    )


class Command(BaseCommand):
    help = 'Just a command for launching a Telegram bot.'

    async def start(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        people = await sync_to_async(create_people)(update.message)

        keyboard = [
            [
                InlineKeyboardButton("Учет денег", callback_data='MONEY'),
                InlineKeyboardButton("Создать штрихкод", callback_data='BARCODE'),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)

        if update.callback_query is not None:
            up = update.effective_message
            await up.edit_text(text="Выберите нужное действие",
                               reply_markup=reply_markup)
        else:
            up = update.message
            # Удаление предъидущего сообщения
            # await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id)
            await up.reply_text(text="Данный бот предназначен для упрощения "
                                     "повседневной жизни, выберите нужное действие",
                                reply_markup=reply_markup)

        return MENU

    async def start_money(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        keyboard = [
            [
                InlineKeyboardButton("Создать движение", callback_data='ADDCASHFLOW'),
                InlineKeyboardButton("Посмотреть историю", callback_data='SHOWHISTORYCASHFLOW'),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.effective_message.edit_text(
            text="Записывайте свои долги с привязкой к человеку и отслеживания истории",
            reply_markup=reply_markup)

        return MONEY

    async def add_cashflow(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        keyboard = [
            [
                InlineKeyboardButton("Назад", callback_data='START'),
                InlineKeyboardButton("Добавить запись", callback_data='ADD'),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.effective_message.edit_text(
            text="Введите запись в формате прим. Петя -20000 Взял в долг",
            reply_markup=reply_markup)

        return MONEY

    async def create_cashflow(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        await sync_to_async(create_cashflow)(update.message)
        await update.effective_message.edit_text(
            text="Запись добавлена",
            reply_markup=add_keyboard())

        return MONEY

    async def show_history_cashflow(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        keyboard = [
            [
                InlineKeyboardButton("Создать движение", callback_data='ADDCASHFLOW'),
                InlineKeyboardButton("Посмотреть историю", callback_data='SHOWHISTORYCASHFLOW'),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)
        await update.effective_message.edit_text(
            text="Записывайте свои долги с привязкой к человеку и отслеживания истории",
            reply_markup=reply_markup)

        return MONEY

    async def create_barcode(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        keyboard = [
            [
                InlineKeyboardButton("Создать QR-COD", callback_data='QR'),
                InlineKeyboardButton("Создать EAN-13", callback_data='EAN13'),
            ]
        ]
        reply_markup = InlineKeyboardMarkup(keyboard)

        if update.callback_query is not None:
            up = update.effective_message
            await up.edit_text(text="Выберите тип"
                                    "штрихкода который хотите создать",
                               reply_markup=reply_markup)
        else:
            up = update.message
            # Удаление предъидущего сообщения
            # await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id)
            await up.reply_text(text="Данный бот предназначен для упрощения "
                                     "создания штрихкодов, выберите тип "
                                     "штрихкода который хотите создать",
                                reply_markup=reply_markup)

        return CREATE_BARCOD

    async def add_ean13(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        await update.effective_message.edit_text(text="Введите EAN-13:",
                                                 reply_markup=add_keyboard())

        return CREATE_EAN

    async def create_ean13(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        up = update.effective_message
        await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id)
        await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id - 1)

        if len(up.text) != 12 or not up.text.isdigit():
            await up.reply_text(text="Введенный код должен состоять из 12 цифр, Введите корректный код:")
        else:
            ean = barcode.get_barcode_class('ean13')
            ean2 = ean(up.text, writer=ImageWriter())
            ean2.save('barcode')
            photo = open('barcode.png', 'rb')
            await up.reply_photo(photo=photo, caption=update.effective_message.text)
            await up.reply_text(text="Введите EAN-13:",
                                reply_markup=add_keyboard())

        return CREATE_EAN

    async def add_qr(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        await update.effective_message.edit_text(text="Введите QR:",
                                                 reply_markup=add_keyboard())

        return CREATE_QR

    async def create_qr(self, update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
        up = update.effective_message
        await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id)
        await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id - 1)
        qrcode = segno.make(up.text, micro=False)
        qrcode.save("qr.png", border=4, scale=10)
        photo = open('qr.png', 'rb')
        await up.reply_photo(photo=photo, caption=update.effective_message.text)
        await up.reply_text(text="Введите QR:",
                            reply_markup=add_keyboard())

        return CREATE_QR

    def handle(self, *args, **kwargs):
        bot = Application.builder().token(API_TOKEN).build()

        conv_handler = ConversationHandler(
            entry_points=[CommandHandler("start", self.start)],
            states={
                MENU: [
                    CallbackQueryHandler(self.create_barcode, pattern="^BARCODE$"),
                    CallbackQueryHandler(self.start_money, pattern="^MONEY$"),
                    CallbackQueryHandler(self.start, pattern="^START$"),
                ],
                MONEY: [
                    CallbackQueryHandler(self.add_cashflow, pattern="^ADDCASHFLOW$"),
                    CallbackQueryHandler(self.show_history_cashflow, pattern="^SHOWHISTORYCASHFLOW$"),
                    MessageHandler(filters.TEXT & ~filters.COMMAND, self.create_cashflow),
                    CallbackQueryHandler(self.start, pattern="^START$"),
                ],
                CREATE_BARCOD: [
                    CallbackQueryHandler(self.add_qr, pattern="^QR$"),
                    CallbackQueryHandler(self.add_ean13, pattern="^EAN13$"),
                ],
                CREATE_EAN: [
                    MessageHandler(filters.TEXT & ~filters.COMMAND, self.create_ean13),
                    CallbackQueryHandler(self.start, pattern="^START$"),
                ],
                CREATE_QR: [
                    MessageHandler(filters.TEXT & ~filters.COMMAND, self.create_qr),
                    CallbackQueryHandler(self.start, pattern="^START$"),
                ],
            },
            fallbacks=[CommandHandler("start", self.start)],
            per_message=False
        )

        bot.add_handler(conv_handler)
        bot.run_polling(allowed_updates=Update.ALL_TYPES)


def add_keyboard():
    keyboard = [
        [
            InlineKeyboardButton("Назад", callback_data='START'),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    return reply_markup
