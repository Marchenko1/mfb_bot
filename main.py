import segno
import barcode

from telegram.ext import (
    Application,
    CommandHandler,
    ContextTypes,
    MessageHandler,
    filters,
    ConversationHandler,
    CallbackQueryHandler,
)
from telegram import Update, InlineKeyboardButton, InlineKeyboardMarkup
from barcode.writer import ImageWriter

API_TOKEN = '6705183255:AAFUSPpYS0OBOGzLS4ngheWQ00nibk7lnUc'


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    keyboard = [
        [
            InlineKeyboardButton("Создать QR-COD", callback_data='QR'),
            InlineKeyboardButton("Создать EAN-13", callback_data='EAN13'),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    if update.callback_query is not None:
        up = update.effective_message
        await up.edit_text(text="Выберите тип"
                                "штрихкода который хотите создать",
                           reply_markup=reply_markup)
    else:
        up = update.message
        # Удаление предъидущего сообщения
        # await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id)
        await up.reply_text(text="Данный бот предназначен для упрощения "
                                 "создания штрихкодов, выберите тип "
                                 "штрихкода который хотите создать",
                            reply_markup=reply_markup)

    return CREATE_BARCOD


async def add_ean13(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.effective_message.reply_text(text="Введите EAN-13:",
                                              reply_markup=add_keyboard())

    return CREATE_EAN


async def create_ean13(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    up = update.effective_message
    await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id)
    await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id - 1)

    if len(up.text) != 12 or not up.text.isdigit():
        await up.reply_text(text="Введенный код должен состоять из 12 цифр, Введите корректный код:")
    else:
        ean = barcode.get_barcode_class('ean13')
        ean2 = ean(up.text, writer=ImageWriter())
        ean2.save('barcode')
        photo = open('barcode.png', 'rb')
        await up.reply_photo(photo=photo, caption=update.effective_message.text)
        await up.reply_text(text="Введите EAN-13:",
                            reply_markup=add_keyboard())

    return CREATE_EAN


async def add_qr(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    await update.effective_message.edit_text(text="Введите QR:",
                                             reply_markup=add_keyboard())

    return CREATE_QR


async def create_qr(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    up = update.effective_message
    await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id)
    await context.application.bot.delete_message(chat_id=up.chat_id, message_id=up.message_id - 1)
    qrcode = segno.make(up.text, micro=False)
    qrcode.save("qr.png", border=4, scale=10)
    photo = open('qr.png', 'rb')
    await up.reply_photo(photo=photo, caption=update.effective_message.text)
    await up.reply_text(text="Введите QR:",
                        reply_markup=add_keyboard())

    return CREATE_QR


def main() -> None:
    bot = Application.builder().token(API_TOKEN).build()

    conv_handler = ConversationHandler(
        entry_points=[CommandHandler("start", start)],
        states={
            CREATE_BARCOD: [
                CallbackQueryHandler(add_qr, pattern="^QR$"),
                CallbackQueryHandler(add_ean13, pattern="^EAN13$"),
            ],
            CREATE_EAN: [
                MessageHandler(filters.TEXT & ~filters.COMMAND, create_ean13),
                CallbackQueryHandler(start, pattern="^START$"),
            ],
            CREATE_QR: [
                MessageHandler(filters.TEXT & ~filters.COMMAND, create_qr),
                CallbackQueryHandler(start, pattern="^START$"),
            ],
        },
        fallbacks=[CommandHandler("start", start)],
        per_message=False
    )

    bot.add_handler(conv_handler)
    bot.run_polling(allowed_updates=Update.ALL_TYPES)


def add_keyboard():
    keyboard = [
        [
            InlineKeyboardButton("Назад", callback_data='START'),
        ]
    ]
    reply_markup = InlineKeyboardMarkup(keyboard)

    return reply_markup


if __name__ == "__main__":
    main()
